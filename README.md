<h1>Lucas Ruiz Belmonte - Proyecto Runner</h1>

<br>
<h3>Requisitos mínimos: </h3>

 1. Personaje con controles por teclado
   - [X] El único control es saltar y se hace con W, SPACE o el input "Fire1" <br>
   (click izquierdo, control izquierdo, botón 0 de mandos [X en play y B en Xbox])

 2. Movimiento por físicas
   - [X] Todo funciona por AddForce

 3. Implementación de colisiones
   - [X] Colisiones y triggers

 4. Condición de derrota con GameOver
   - [X] Sí y con botón de reinicio de escena

 5. Instanciación y Destrucción de objetos por código
   - [X] El background y los obstáculos están hechos con instanciación y destrucción y las plataformas por object pooling

 6. Control temporal a partir de funciones como Invoke o InvokeRepeating
   - [X] El background funciona con InvokeRepeating

 7. Concepteo de Aleatoriedad
   - [X] La Y de los obstáculos

 8. Puntuación mostrada por pantalla con un canvas
   - [X] Tanto mientras juegas como en el GameOver

 9. Movimento de la cámara
   - [X] No hace falta pero he puesto una animación algo leve para que no sea del todo estático

<br><hr><br>

<h3>Requisitos adicionales:</h3>

 1. Dificulad aumenta con el tiempo
   - [X] La Dificulad aumenta cada vez que pasas por una tubería hasta un limite

 2. Paralax
   - [X] He hecho un paralax utilizando velocidades aleatorias en las nubes

 3. Singleton
   - [X] Todo funciona a través del script RunnerManager, este es un singleton para poder acceder más rápidamente a todo

 4. Animaciones
   - [X] Animación para saber como comenzar y de cámara

 5. Todo reescalable
   - [X] Los bloques, los obstáculos y el canvas se reescalan al tamaño de la cámara
   
 6. Referencias
   - [X] Juego con los sprites de mi primer juego por celebración de 4o aniversario xD
