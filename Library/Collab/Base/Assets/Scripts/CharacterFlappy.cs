﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterFlappy : MonoBehaviour {

    bool started = false;
    bool canJump = true;
    Rigidbody2D rb;

    public GameObject loseCanvas;
    public GameObject scoreText;
    public GameObject scoreTextDead;

    public float jumpForce = 10f;

    void Start() {
        rb = GetComponent<Rigidbody2D>();

        started = false;
        canJump = true;

        loseCanvas = GameObject.Find("LoseCanvas");
        scoreText = GameObject.Find("ScoreText");
        scoreTextDead = GameObject.Find("ScoreTextDead");

        rb.gravityScale = 0f;
        rb.velocity = Vector3.zero;
        scoreText.SetActive(false);
        loseCanvas.SetActive(false);
    }

    void Update() {
        if (canJump && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W) || Input.GetMouseButtonDown(0)))
            jump();
    }


    private void jump() {
        if (!started) {
            scoreText.SetActive(true);
            started = true;
            rb.gravityScale = 1f;
            RunnerManager.instance.floorVelocity = 3f;
        }

        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.up*jumpForce*10, ForceMode2D.Impulse);
    }

    #region coll
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.CompareTag("Obstacle")) {
            hitto();
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.transform.CompareTag("Floor")) {
            hitto();
        }

    }

    public void changeScore(int i) {
        scoreText.GetComponent<TMPro.TextMeshProUGUI>().text = "SCORE: " + i;
        scoreTextDead.GetComponent<TMPro.TextMeshProUGUI>().text = "SCORE: " + i;
    }

    private void hitto() {
        scoreText.SetActive(false);
        canJump = false;
        RunnerManager.instance.floorVelocity = 0;
        loseCanvas.SetActive(true);
    }
    #endregion
}
