﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerPiece : MonoBehaviour {

    public float floorWidth;

    void Start() {
        //El tamaño del bloque del ancho necesario para la pantalla utilizada
        loadFloorWidth();

    }

    void FixedUpdate(){
        //Muevo el terreno hacia la izquierda y si llega a X valor lo resposiciono 4 plataformas a la 
        //derecha para que haga movimiento seamless
        transform.Translate(new Vector3(-RunnerManager.instance.floorVelocity*Time.deltaTime, 0, 0));

        if(transform.position.x <= -floorWidth * 2) {
            transform.position = new Vector3(transform.position.x + ( floorWidth * 4 ), transform.position.y,
                transform.position.z);
            removeObstacles();
            generateObstacle();
            loadFloorWidth();
        }
    }

    private void generateObstacle() {
        int num = Random.Range(0, RunnerManager.instance.obstacles.Length);
        if (RunnerManager.instance.obstacles[num] != null) {
            //Creo aleatorio entre la lista de obstaculos
            GameObject obs = (GameObject)Instantiate(RunnerManager.instance.obstacles[num]);
            Obstacle scrObs = obs.GetComponent<Obstacle>();

            //pillo el maximo y minimo de la pantalla y lo paso a formato || Lo mas alto = 1 | Lo más bajo = -1
            float maxTval = ( scrObs.maxY + 1 ) / 2; // t
            float minTval = ( scrObs.minY + 1 ) / 2; // t

            //traspaso los numeros "locales" a unidades de unity
            float maxScreen = RunnerManager.instance.camSize.y / 3;
            float minScreen = ( -RunnerManager.instance.camSize.y / 3 ) +
                (RunnerManager.instance.camSize.y / (RunnerManager.instance.floorHeight*1.5f) );

            //como el parent es la escena lo posiciono en y
            obs.transform.localPosition = new Vector3(
                0,
                Random.Range(
                    Mathf.Lerp(minScreen, maxScreen, minTval),
                    Mathf.Lerp(minScreen, maxScreen, maxTval)),
                0);

            //Lo meto en el parent de la plataforma y lo centro a esta
            obs.transform.parent = transform.GetChild(1);
            obs.transform.localPosition = new Vector3(0, obs.transform.localPosition.y, 0);
        }
    }

    private void removeObstacles() {
        //Borro los hijos del child de obstaculos
        for (int i = 0; i < transform.GetChild(1).childCount; i++) {
            GameObject.Destroy(transform.GetChild(1).GetChild(i).gameObject);
        }
    }

    public void loadFloorWidth() {
		//le pone el ancho que debería a la plataforma
        transform.GetChild(0).transform.localScale = new Vector3(floorWidth,
                transform.transform.localScale.y*4, 1);
    }
}
