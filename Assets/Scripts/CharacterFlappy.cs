﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterFlappy : MonoBehaviour {

    bool started = false;
    bool canJump = true;
	bool isAxisDown = false;
    Rigidbody2D rb;

    public GameObject loseCanvas;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI scoreTextDead;
	public GameObject clickToStart;

    public float jumpForce = 10f;

    void Start() {
        rb = GetComponent<Rigidbody2D>();

        started = false;
        canJump = true;

		//Pillar a lo sucio todo lo del canvas
        loseCanvas = GameObject.Find("LoseCanvas");
        scoreText = GameObject.Find("ScoreText").GetComponent<TextMeshProUGUI>();
        scoreTextDead = GameObject.Find("ScoreTextDead").GetComponent<TextMeshProUGUI>();
		clickToStart = GameObject.Find("ClickToStart");

		//inicializar todas las variables a sus estados de inicio
        rb.gravityScale = 0f;
        rb.velocity = Vector3.zero;
        scoreText.gameObject.SetActive(false);
        loseCanvas.SetActive(false);
		clickToStart.SetActive(true);
    }

    void Update() {
		if (canJump && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W))) 
			jump();

		//Las cosas raras en los axis es para que funcione para mandos y como no tiene un getAxisDown pues se hace a mano
		if (Input.GetAxisRaw("Fire1") != 0 && !isAxisDown) {
			isAxisDown = true;
			jump();
		}

		if (Input.GetAxisRaw("Fire1") == 0)
			isAxisDown = false;

    }


    private void jump() {
		//si no ha dado su primer click se quita el "pulsa para comenzar", la gravedad empieza a afectar y todo empieza a moverse
        if (!started) {
            scoreText.gameObject.SetActive(true);
			clickToStart.SetActive(false);
			started = true;
            rb.gravityScale = 1f;
            RunnerManager.instance.floorVelocity = 3f;
        }

		//al saltar quito primero la velocidad para que no afecten inercias
        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.up*jumpForce*10, ForceMode2D.Impulse);
    }

    public void scoreUp() {
		//sube el score y cambia los textos de scores
        RunnerManager.instance.score++;
        int i = RunnerManager.instance.score;
        scoreTextDead.SetText("SCORE: {0}", i);
        scoreText.SetText("SCORE: {0}", i);
    }

    #region coll
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.CompareTag("Obstacle")) {
            hitto();
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.transform.CompareTag("Floor")) {
            hitto();
        }

    }

    private void hitto() {
		//Activa gameover, desactiva el score normal y quita la velocidad y capacidad de saltar
        scoreText.gameObject.SetActive(false);
        canJump = false;
        RunnerManager.instance.floorVelocity = 0;
        loseCanvas.SetActive(true);
    }
    #endregion
}
